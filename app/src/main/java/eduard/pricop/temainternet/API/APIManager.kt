package eduard.pricop.temainternet.API

import eduard.pricop.temainternet.App
import eduard.pricop.temainternet.Model.Data
import retrofit2.Callback

object APIManager {

    fun getPhotos(callBack: Callback<List<Data>>) {
        App.instance!!.retrofit.create(APIService::class.java)
            .getPhotos()
            .enqueue(callBack)
    }

}