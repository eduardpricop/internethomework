package eduard.pricop.temainternet.API


import eduard.pricop.temainternet.Model.Data
import retrofit2.Call
import retrofit2.http.GET

interface APIService {

    @GET("EduardPricop/internet/main/json.txt")
    fun getPhotos(): Call<List<Data>>

}