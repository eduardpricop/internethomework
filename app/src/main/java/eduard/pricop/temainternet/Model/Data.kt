package eduard.pricop.temainternet.Model

class Data {

    var id: Int? = null
    var name: String? = null
    var url: String? = null
    var width: Int? = null
    var height: Int? = null
    var box_count: Int? = null

}