package eduard.pricop.temainternet

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import eduard.pricop.temainternet.API.APIManager
import eduard.pricop.temainternet.Adapter.MyAdapter
import eduard.pricop.temainternet.Model.Data
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), Callback<List<Data>> {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        APIManager.getPhotos(this)
        recyclerList.layoutManager = LinearLayoutManager(this)
        recyclerList.hasFixedSize()

    }

    override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
        if (response.isSuccessful) {
            if (!response.body().isNullOrEmpty()) {
                val adapter = MyAdapter(response.body()!!)
                recyclerList.adapter = adapter
            }
        } else {

        }
    }

    override fun onFailure(call: Call<List<Data>>, t: Throwable) {
        Toast.makeText(this, "Error occurred", Toast.LENGTH_SHORT).show()
    }
}