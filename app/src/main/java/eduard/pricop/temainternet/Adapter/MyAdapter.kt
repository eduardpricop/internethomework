package eduard.pricop.temainternet.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import eduard.pricop.temainternet.Adapter.MyAdapter.MyViewHolder
import eduard.pricop.temainternet.Model.Data
import eduard.pricop.temainternet.R
import kotlinx.android.synthetic.main.row.view.*

class MyAdapter(private val list: List<Data>) : RecyclerView.Adapter<MyViewHolder>() {

    inner class MyViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.row, parent, false)
    ) {
        fun bind(data: Data) = with(itemView) {
            titleView.text = data.name
            widthView.text = data.width.toString()
            heightView.text = data.height.toString()
            Glide.with(context)
                .load(data.url)
                .fitCenter()
                .into(urlView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyViewHolder(parent)

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount() = list.size

}